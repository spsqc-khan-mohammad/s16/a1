const q1Select = document.querySelector("#q1");
const q2Select = document.querySelector("#q2");
const a1Span = document.querySelector("#a1");
const a2Span = document.querySelector("#a2");
const submitButton = document.querySelector("#submit-button");
const firstNameInput = document.querySelector("#first-name");
const lastNameInput = document.querySelector("#last-name");

const correctAnswers = {
  q1: "wolf",
  q2: "boy",
};

const handleQuestion = (event, correctAnswer, answerSpan) => {
  const selectedValue = event.target.value;
  const isCorrect = selectedValue === correctAnswer;

  answerSpan.textContent = isCorrect ? "Correct!" : "Incorrect!";
  answerSpan.classList.toggle("text-success", isCorrect);
  answerSpan.classList.toggle("text-danger", !isCorrect);
};

q1Select.addEventListener("change", (event) => {
  handleQuestion(event, correctAnswers.q1, a1Span);
});

q2Select.addEventListener("change", (event) => {
  handleQuestion(event, correctAnswers.q2, a2Span);
});

submitButton.addEventListener("click", () => {
  const numCorrect = [q1Select, q2Select].reduce((count, select) => {
    return select.value === correctAnswers[select.id] ? count + 1 : count;
  }, 0);

  const message = `Good day, ${firstNameInput.value} ${lastNameInput.value}. You have answered ${numCorrect} out of 2 questions correctly.`;
  alert(message);
});
